---
title: "STAT 510 | HW 11"
author: "Ricardo Batista"
date: "4/18/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(lme4); library(lmerTest); library(nlme)
```

# Question 1

&nbsp;&nbsp;&nbsp;&nbsp; The model implied by the set up is $Y = \beta X + Zw + e$ where

$$
\begin{aligned}
\beta &= (\mu_{11}, \dots, \mu_{14}, \mu_{21}, \dots, \mu_{24}, \mu_{31}, \dots, \mu_{34})'
\\[10pt]
X &= \underset{3\times3}{I} \otimes \left(\underset{5\times1}{1} \otimes \underset{4\times4}{I}\right)
\\[10pt]
w &= (w_{11}, \dots, w_{15}, w_{21}, \dots, w_{25}, w_{31}, \dots, w_{35})'
\\[10pt]
Z &= \underset{15 \times 15}{I} \otimes \underset{4 \times 1}{1}.
\end{aligned}
$$


```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
data <- read_delim("https://dnett.github.io/S510/HeartRate.txt", delim = "\t", 
                   col_types = "nfff") %>% as.data.frame()
n <- nrow(data)
w <- nlevels(data$drug)
s <- nlevels(data$time)
nwomen <- n/(w*s)
```


## (a)

$$
\begin{aligned}
Var(Y) 
= Z Var(w) Z' + Var(e)
= \sigma_w^2Z Z' + \sigma_e^2 \underset{60 \times 60}{I}
&=
\sigma_w^2
\left(\underset{15 \times 15}{I} \otimes \underset{4 \times 1}{1}\right)
\left(\underset{15 \times 15}{I} \otimes \underset{4 \times 1}{1'}\right)
+ \sigma_e^2 \underset{60 \times 60}{I}
\\[10pt]
&=
\sigma_w^2
\underset{15 \times 15}{I} \otimes \underset{4 \times 4}{11'}
+ \sigma_e^2 \underset{15 \times 15}{I} \otimes \underset{4 \times 4}{I}
\\[10pt]
&= \underset{15 \times 15}{I} \otimes
\left(\sigma_w^2 \underset{4 \times 4}{11'} + \sigma_e^2 \underset{4 \times 4}{I}\right)
\end{aligned}
$$

Alternatively -- and more intuitively --, we could've worked in the oppostive direction, realizing that $Var(Y)$ is a block diagonal matrix with block equal to $\left(\sigma_w^2 \underset{4 \times 4}{11'} + \sigma_e^2 \underset{4 \times 4}{I}\right)$.  

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; We know the following about the "no interactions" null hypothesis:

$$
\begin{aligned}
&H_0 = \mu_{ik} - \mu_{ik^*} - \mu_{i^*k} + \mu_{i^*k^*} = 0 && \forall i \neq i^*, k \neq k^*
\\[10pt]
\iff&
H_0 = \mu_{ik} - \bar\mu_{i\cdot} - \bar\mu_{\cdot k} + \mu_{\cdot \cdot} = 0 && \forall i, k
\end{aligned}
$$

Thus, 

$$
\frac{1}{(3 - 1)(4 - 1)} \sum_{i = 1}^3 \sum_{k = 1}^4 
\left(\mu_{ik} - \bar\mu_{i\cdot} - \bar\mu_{\cdot k} + \mu_{\cdot \cdot}\right)^2
= 0
$$

is equivalent to no interactions between treatment and time. As such, we can run the test two different ways:

&nbsp;&nbsp;&nbsp;&nbsp; $\underline{\text{ANOVA}}:$

[i = treatment; t = 3]
[j = woman; n = 5]
[k = time; m = 4]

Sum of squares

$$

$$

&nbsp;&nbsp;&nbsp;&nbsp; 





```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
Source <- c("Treatment", "Woman(trt)", "Time", "Treatment X Time", "Error", "C. Total")
DF <- c(w - 1, w*(nwomen - 1), s - 1, (w - 1)*(s - 1), (s - 1)*w*(nwomen - 1), n - 1)
SS <- c("4\sum_{i = 1}^3 (\bar{y_{i\cdot \cdot}}")
EMS <- c("$4\\sigma_w^2 + \\sigma_e^2 + \tfrac{4}{3 - 1} \\sum_{i = 1}^3(\bar{\mu}_{i \cdot} - \bar{\mu}_{\cdot \cdot}$",
        )
ANOVA <- data.frame(,
                    )
```

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}

fit1 <- lme(y ~ drug * time, random = ~ 1 | woman, data = data)
fit1 <- lmer(y ~ drug * time + (1 | woman), data = data)
anova(fit1)

```




&nbsp;&nbsp;&nbsp;&nbsp; $\underline{C\hat\beta}:$

&nbsp;&nbsp;&nbsp;&nbsp; Recall that, for an appropriate $C$ matrix

$$
$$

is an $F$ statistic we can use to test the foregoing hypothesis. Recall $\hat\beta_{\Sigma} = (X'\Sigma^{-1}X)^{-}X'\Sigma^{-1}y$, and the matrix in question is

$$
C
=
\begin{bmatrix}
\end}bmatrix}
$$


## (c)

## (d)


# Question 2

## (a)

## (b)

## (c)

## (d)

## (e)

## (f)

## (g)

## (h)

## (i)


# Question 3

## (a)

## (b)

## (c)

## (d)

## (e)

## (f)



